import React from 'react';
import PropTypes from 'prop-types';
import stone from'../../../../static/img/NaturalStone.jpg';

import './MainDescription.scss'

const MainDescription = ({text}) => {
    return ( 
        <div className={`mainDescription`}>
            <div className="mainDescription__poligon" style={{
                backgroundImage : `url(${stone})`
            }}></div>
            <div className="mainDescription__text">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                <span>ReactJS</span>
                Cumque cum accusantium magnam quod accusamus.
                Ex maxime nam provident magni odio sapiente maiores quas,
                officiis inventore minima numquam, odit ab <span>eos</span>.
            </div>
        </div>
     );
}

MainDescription.propTypes = {
};

MainDescription.defaultProps = {
};
 
export default MainDescription;