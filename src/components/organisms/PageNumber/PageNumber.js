import React from 'react';
import PageNumberCount from '../../molecules/PageNumberCount/PageNumberCount';

import PropTypes from 'prop-types';

import './PageNumber.scss'

const PageNumber = ({pageNumb, pagesCount}) => {
    return ( 
        <div className="pageNumber__wrap">
            <div className="pageNumber__text">Page</div>
            <PageNumberCount pageNumb={pageNumb} pagesCount={pagesCount}/>
        </div>
     );
}

PageNumber.propTypes = {
    pageNumb: PropTypes.number,
    pagesCount: PropTypes.number
};

PageNumber.defaultProps = {
    pageNumb: 1
};
 
export default PageNumber;