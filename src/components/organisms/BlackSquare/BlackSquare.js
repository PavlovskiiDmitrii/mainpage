import React from 'react';
import PropTypes from 'prop-types';

import main from'../../../../static/img/BlackSquare.jpg';

import './BlackSquare.scss'

const BlackSquare = ({side}) => {
    return ( 
        <div className={`blackSquare ${side === 'right' ? 'blackSquare_right' : ''}`}>
            <img src={main} className={`blackSquare__img`} alt=""/>
            <div className="blackSquare__text">Не такой уж он и черный...</div>
            <a target="_blank" href='https://ru.wikipedia.org/wiki/%D0%A7%D1%91%D1%80%D0%BD%D1%8B%D0%B9_%D0%BA%D0%B2%D0%B0%D0%B4%D1%80%D0%B0%D1%82'
                className="blackSquare__link">Черный квадрат Казимира Малевича</a>
        </div>
     );
}

BlackSquare.propTypes = {
};

BlackSquare.defaultProps = {
};
 
export default BlackSquare;