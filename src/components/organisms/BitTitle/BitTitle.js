import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import './BitTitle.scss'

const BitTitle = ({text}) => {

    
    const [y, setY] = useState(0);
    const [x, setX] = useState(0);
    const [mouse, setMouse] = useState(null);
   
    const [transition, setTransition] = useState(0.2);
    const [move, setMove] = useState(null);

    const elem = useRef(null);

    useEffect(() => {
        if (move === true) {
            const id = setInterval(() => {
                let elemProps = elem.current.getBoundingClientRect();
                setX((window.mouser.x - elemProps.x - elemProps.width/2)/2);
                setY((window.mouser.y - elemProps.y - elemProps.height/2)/2);
            }, 20);
            return () => clearInterval(id);
        }
    },[move]);

    useEffect(() => {
        if (mouse !== null) {
            window.mouser = mouse
        }
        if (mouse === null) {
            window.mouser = null;
            setTransition(0.2)
        }
    },[mouse]);

    const onMouseMove = (e) => {
        setMouse({x:e.clientX, y:e.clientY });
    }

    const onMouseLeave = () => {
        setMove(null);
        setMouse(null);
        setTransition(0.2);
        setY(0);
        setX(0);
    }

    const onMouseEnter = () => {
        setMove(true);
        setTimeout(() => {
            if (window.mouser) setTransition(.15);
        }, 100);
        setTimeout(() => {
            if (window.mouser) setTransition(.1);
        }, 200);
        setTimeout(() => {
            if (window.mouser) setTransition(0);
        }, 300);
    }

    return ( 
        <div className={`bitTitle`} 
            onMouseMove={onMouseMove}
            onMouseLeave={onMouseLeave}
            onMouseEnter={onMouseEnter}>
            <div ref={elem} className="bitTitle__before" style={{
                transform: `translateX(${-x/5}px) translateY(${-y/5}px)`,
                transition: `${transition}s`
            }}>
                {text}
            </div>
            <div className="bitTitle__text" style={{
                transform: `translateX(${x/5}px) translateY(${y/5}px)`,
                transition: `${transition}s`
            }}>
                {text}
            </div>
        </div>
     );
}

BitTitle.propTypes = {
    text : PropTypes.string
};

BitTitle.defaultProps = {
    text : 'PAVLSK.ru'
};
 
export default BitTitle;