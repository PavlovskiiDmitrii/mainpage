import React from 'react';
import MenuItem from '../../molecules/MenuItem/MenuItem'

import PropTypes from 'prop-types';

import './Menu.scss'

const Menu = () => {
    return ( 
        <div className="menu__wrap">
            <div className="menu">
                <MenuItem text={'Главная'} />
                <MenuItem text={'Компоненты'} height={140}/>
                <MenuItem text={'Контакты'} />
            </div>
        </div>
     );
}

Menu.propTypes = {
};

Menu.defaultProps = {
};
 
export default Menu;