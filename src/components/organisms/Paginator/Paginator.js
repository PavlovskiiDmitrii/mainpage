import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './Paginator.scss'

const Paginator = (props) => {
    const { pageNumb, pagesCount, setPageNumb } = props;

    const [ translateY, setTranslateY ] = useState(0);
    
    const createArr = () => {
        let arr = [];
        for (let i = 0; i < pagesCount; i++) {
            arr[i] = i
        }
        return arr;
    }

    useEffect(() => {
        setTranslateYSide(pageNumb);
    }, [pageNumb]);


    const setTranslateYSide = (pageNumb) => {
        let clientWidth = document.documentElement.clientWidth;
        if (clientWidth > 1900) {
            setTranslateY((pageNumb - 1)*40 + 20)
        }
        if (clientWidth < 1900 && clientWidth > 1200 ) {
            setTranslateY((pageNumb - 1)*35 + 15)
        }
        if (clientWidth <= 1200 && clientWidth > 767 ) {
            setTranslateY((pageNumb - 1)*36 + 10)
        }
    }

    return ( 
        <div className="paginator__wrap">
            <div className="paginator">
                {
                    createArr().map((i) => (
                        <div key={i} className="paginator__item" onClick={() => {setPageNumb(i+1)}}></div> 
                    ))
                }
                <div style={{transform: `translateY(${translateY}px)`}} className="paginator__item paginator__item_activate">
                
                </div> 
            </div>
        </div>
     );
}

Paginator.propTypes = {
    pageNumb: PropTypes.number,
    pagesCount: PropTypes.number,
    setPageNumb: PropTypes.func
};

Paginator.defaultProps = {
};
 
export default Paginator;