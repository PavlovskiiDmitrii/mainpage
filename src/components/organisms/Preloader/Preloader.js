import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './Preloader.scss'

const Preloader = props => {
    const { text, timeAfterStart, timeAfterDeactivate, activate } = props;

    const preloaderElem = useRef(null);
    const preloaderWrap = useRef(null);
    const [fadeProloader, setFadeProloader] = useState(false);
    const [hiddenProloader, setHiddenProloader] = useState(false);

    useEffect(() => {
        if (activate) {
            preparePreload();
            startPreload(timeAfterStart, timeAfterDeactivate);
        }
    }, []);
    
    const preparePreload = () => {
        let text = preloaderElem.current.innerHTML;
        preloaderElem.current.innerHTML = ''

        for (let i = 0; i < text.length; i++) {
            let span = document.createElement('span');
            span.classList.add("preloader__char");
            span.innerHTML = text[i];
            preloaderElem.current.append(span);
        }
    }

    const startPreload = (timeAfterStart, timeAfterDeactivate) => {
        let childNodesLength = preloaderElem.current.childNodes.length;
        setTimeout(() => {
            for (let i = 0; i < childNodesLength; i++) {
                setTimeout(() => {
                    preloaderElem.current.childNodes[i].classList.add("preloader__char_activate")
                }, 100 * i);
                if (i+1 === childNodesLength) {
                    endPreloader(timeAfterDeactivate, i);
                }
            }
        }, timeAfterStart);
    }

    const endPreloader = (timeAfterDeactivate, i) => {
        setTimeout(() => {
            setFadeProloader(true);
            setTimeout(() => { setHiddenProloader(true) }, 500)
        }, timeAfterDeactivate + 100 * i)
    }

    return (
        activate ? 
            (<div className={`preloader__wrap ${fadeProloader ? 'preloader__wrap_fade' : ''} ${hiddenProloader ? 'preloader__wrap_hidden' : ''}`} ref={preloaderWrap}>
                <div className="preloader" ref={preloaderElem}>
                    {text}
                </div>
            </div>)
        :
        ''
     );
}

Preloader.propTypes = {
    text: PropTypes.string,
    timeAfterStart: PropTypes.number,
    timeAfterDeactivate: PropTypes.number,
    activate: PropTypes.bool
};

Preloader.defaultProps = {
    text: "pvlvsk.ru",
    timeAfterStart: 1000,
    timeAfterDeactivate: 2000,
    activate: true,
};
 
export default Preloader;