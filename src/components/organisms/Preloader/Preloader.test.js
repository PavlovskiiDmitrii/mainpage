import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import Preloader from './Preloader';

configure({adapter: new Adapter()});

const setUp = (props) => shallow(<Preloader {...props}/>);

describe('<Preloader />', () => {

  it('should render component activate', () => {
    const component = shallow(<Preloader activate={true}/>);
    const wrapper = component.find(".preloader__wrap");
    expect(wrapper.length).toBe(1);
  });

  it('should render component not activate', () => {
    const component = shallow(<Preloader activate={false}/>);
    const wrapper = component.find(".preloader__wrap");
    expect(wrapper.length).toBe(0);
  });

  

  //Snapshot
  it('should render component', () => {
    const component = render(<Preloader/>);
    expect(component).toMatchSnapshot();
  });
});