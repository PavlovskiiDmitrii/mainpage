import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import PageContent1 from '../../PagesContent/Page1/index.js';

import Page from '../../molecules/Page/Page';

import './Side.scss'

const Side = (props) => {
    const {side, pageNumb, pagesCount} = props;

    const [ translateY, setTranslateY ] = useState(0);
    const [ scale, setScale ] = useState(1);

    const SCALE_VALUES = 0.85;

    useEffect(() => {
        setTranslateYSide(side, pageNumb)
        setScaleSide(SCALE_VALUES)
    }, [pageNumb]);

    const setScaleSide = (scale) => {
        setScale(scale);
        setTimeout(() => {
            setScale(1);
        }, 500)
    }
    const setTranslateYSide = (side, pageNumb) => {
        if (side === 'left') setTranslateY(((pageNumb - 1 ) * 100) + 100);
        else if (side === 'right') setTranslateY(((pagesCount - pageNumb + 1) * 100));
    }

    return (
        <div id="count-up" className={`side__wrap side__wrap_${side}`}>
            <div className={`side side_${side}`} style={{transform: `translateY(-${translateY}vh) scale(${scale})`}}>
                <Page content={<PageContent1 side={side}/>}/>
                <Page content={<div>2</div>}/>
                <Page content={<div>3</div>}/>
                <Page content={<div>4</div>}/>
            </div>
        </div>
     );
}

Side.propTypes = {
    side: PropTypes.oneOf(['left', 'right']),
    pageNumb: PropTypes.number,
    pagesCount: PropTypes.number
};

Side.defaultProps = {
    pageNumb: 1
};
 
export default Side;