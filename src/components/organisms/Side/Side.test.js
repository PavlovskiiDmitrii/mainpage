import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import Side from './Side';

configure({adapter: new Adapter()});

describe('<Side />', () => {
  
  it('render left side', () => {
    const component = shallow(<Side side={'left'}/>);

    const wrapperCorrect = component.find(".side__wrap_left");
    const wrapperUnCorrect = component.find(".side__wrap_right");

    expect(wrapperCorrect.length).toBe(1);
    expect(wrapperUnCorrect.length).toBe(0);
  })

  it('render right side', () => {
    const component = shallow(<Side side={'right'}/>);

    const wrapperCorrect = component.find(".side__wrap_right");
    const wrapperUnCorrect = component.find(".side__wrap_left");
    
    expect(wrapperCorrect.length).toBe(1);
    expect(wrapperUnCorrect.length).toBe(0);
  })  

  //Snapshot
  it('should render component', () => {
    const component = render(<Side/>);
    expect(component).toMatchSnapshot();
  });
});