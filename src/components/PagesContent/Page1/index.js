import React from 'react';
import Left from './Left/TemplateLeft';
import Right from './Right/TemplateRight';

const PageContent = (props) => {
    const {side} = props

    return (
        <React.Fragment>
            {
                side === 'left'
                ?
                <Left/>
                :
                <Right/>
            }
        </React.Fragment>
     );
}
 
export default PageContent;