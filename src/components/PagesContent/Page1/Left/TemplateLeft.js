import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ToggleVision from '../../../molecules/ToggleVision/ToggleVision';
import BitTitle from '../../../organisms/BitTitle/BitTitle';
import MainDescription from '../../../organisms/MainDescription/MainDescription';

import './TemplateLeft.scss'

const TemplateLeft = () => {

    const [triger, setTriger] = useState(false);

    return ( 
        <React.Fragment>
            <ToggleVision close={triger}>
                <BitTitle text={'Pvlvsk.ru'}/>
            </ToggleVision>

        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 'calc(50vw - 40px)',
            height: 'calc(70vh - 40px)'
        }}>
            <ToggleVision close={triger}>
                <MainDescription/>
            </ToggleVision>
        </div>
            
        </React.Fragment>
     );
}

TemplateLeft.propTypes = {
};

TemplateLeft.defaultProps = {
};
 
export default TemplateLeft;