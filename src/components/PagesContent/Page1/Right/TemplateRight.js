import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ToggleVision from '../../../molecules/ToggleVision/ToggleVision';
import BlackSquare from '../../../organisms/BlackSquare/BlackSquare';

import './TemplateRight.scss'

const TemplateRight = () => {
    
    const [triger, setTriger] = useState(false);

    return ( 
        <React.Fragment>
                <ToggleVision close={triger}>
                   <BlackSquare side={'right'}/>
                </ToggleVision>
        </React.Fragment>
     );
}

TemplateRight.propTypes = {
};

TemplateRight.defaultProps = {
};
 
export default TemplateRight;