import React, { useState, useEffect } from 'react';
import Paginator from '../../organisms/Paginator/Paginator';
import Menu from '../../organisms/Menu/Menu';
import PageNumber from '../../organisms/PageNumber/PageNumber';
import ScrollPage from '../../molecules/ScrollPage/ScrollPage'

import ToggleContext from '../../context/Context';

import Side from '../../organisms/Side/Side';

import './Content.scss'

const Content = () => {

    const [ pageNumb, setPageNumb ] = useState(1);
    const [ toggle, setToggle ] = useState(true);
    const pagesCount = 4;

    useEffect(() => {
        setToggle(true);
        setTimeout(() => {
            setToggle(false);
        }, 1000)
    }, [pageNumb])

    const upHandler = () => {
        if (pageNumb > 1) {
            setToggle(true);
            setTimeout(() => {
                setPageNumb(pageNumb - 1)
            }, 1000)
        }
    }
    const downHandler = () => {
        if (pageNumb < pagesCount) {
            setToggle(true);
            setTimeout(() => {
                setPageNumb(pageNumb + 1)
            }, 1000)
        }
    }

    return (
        <ScrollPage 
            upHandler={upHandler}
            downHandler={downHandler} 
            delay={1000}>
                <section className="content">
                    <Paginator pageNumb={pageNumb} pagesCount={pagesCount} setPageNumb={setPageNumb}/>
                    <ToggleContext.Provider value={toggle}>
                        <Side side='left' pageNumb={pageNumb} pagesCount={pagesCount}></Side>
                        <Side side='right' pageNumb={pageNumb} pagesCount={pagesCount}></Side>
                    </ToggleContext.Provider>
                    
                    <Menu/>
                    <PageNumber pageNumb={pageNumb} pagesCount={pagesCount}/>
                </section>
        </ScrollPage>
     );
}
 
export default Content;