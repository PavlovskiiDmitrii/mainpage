import React from "react";
import Content from '../templates/Content/Content';
import Preloader from '../organisms/Preloader/Preloader';

import './App.scss';

const App = () => {
  
    return (
        <div className="app-wrap">
            <Content/>
            <Preloader text="pvlvsk.ru" activate={true}/>
        </div>
    );
  };

export default App;