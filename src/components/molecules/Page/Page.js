import React from 'react';

import './Page.scss'

const Page = ({content}) => {
    return ( 
        <div className={`page`}>
            {content}
        </div>
     );
}
 
export default Page;