import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import PageNumberCount from '../../molecules/PageNumberCount/PageNumberCount';

configure({adapter: new Adapter()});

describe('<PageNumberCount />', () => {
  
  it('checking render the number of items', () => {
    const component = shallow(<PageNumberCount pagesCount={6}/>);
    const wrapper = component.find(".pageNumberCount__item");
    expect(wrapper.length).toBe(6);
  })

  it('checking render the 0 items', () => {
    const component = shallow(<PageNumberCount pagesCount={0}/>);
    const wrapper = component.find(".pageNumberCount__item");
    expect(wrapper.length).toBe(0);
  })

  it('renders correctly', () => {
    const wrapper = shallow(<PageNumberCount pagesCount={4} pageNumb={1}/>);

    expect(wrapper).toMatchSnapshot();
  })

});