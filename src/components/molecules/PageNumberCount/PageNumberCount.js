import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './PageNumberCount.scss'

const PageNumberCount = ({pageNumb, pagesCount}) => {
    const [ translateY, setTranslateY ] = useState(0);

    const createArr = (pagesCount) => {
        let arr = [];
        for (let i = 0; i < pagesCount; i++) {
            arr[i] = i
        }
        return arr;
    }

    useEffect(() => {
        setTranslateY((pageNumb - 1) * -60 - 5)
    }, [pageNumb])

    return ( 
        <div className="pageNumberCount__wrap">
            <div className="pageNumberCount" style={{transform: `translateY(${translateY}px)`}}>
                {
                    createArr(pagesCount).map((i) => (<div className="pageNumberCount__item" key={i}>{i + 1}</div>))
                }
            </div>
        </div>
     );
}

PageNumberCount.propTypes = {
    pageNumb: PropTypes.number,
    pagesCount: PropTypes.number
};

PageNumberCount.defaultProps = {
    pageNumb: 1
};
 
export default PageNumberCount;