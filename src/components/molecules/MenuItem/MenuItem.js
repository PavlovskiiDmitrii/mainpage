import React from 'react';
import PropTypes from 'prop-types';

import './MenuItem.scss'

const MenuItem = (props) => {
    const {text, height} = props

    return ( 
        <div className="menuItem" style={{
            height: height
        }}>
            <div className="menuItem__text">{text}</div>
            <div className="menuItem__elem" onClick={() => {console.log(123)}}>{text}</div>
        </div>
     );
}

MenuItem.propTypes = {
    text: PropTypes.string,
    height: PropTypes.number
};

MenuItem.defaultProps = {
    text: 'ля-ля-ля',
    height: 80
};
 
export default MenuItem;