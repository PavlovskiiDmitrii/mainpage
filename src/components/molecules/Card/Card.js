import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import defaultCard from '../../../../public/img/NotFoto.png'

import './Card.scss'

const Card = (props) => {
    const { title, description, link, img } = props;

    const [cardOpen, setCardOpen] = useState(false);
    const card = useRef(null);

    const documentWidth = document.documentElement.clientWidth;
    const documentHeight = document.documentElement.clientHeight;

    const openCard = () => {
        setCardOpen(!cardOpen);

        setTimeout(() => {
            location.href = `${link}`;
        }, 2500) 
    }

    useEffect(() => {
        if (cardOpen) {
            cardExpansionAllScreen(card.current);
        }
    }, [cardOpen]);

    const cardExpansionAllScreen = (elem) => {
        let cardCoordinates = elem.getBoundingClientRect();

        elem.style.left = `-${cardCoordinates.x + 10}px`;
        elem.style.top = `-${cardCoordinates.y + 10}px`;
        elem.style.right = `-${documentWidth - (cardCoordinates.x + cardCoordinates.width - 10)}px`;
        elem.style.bottom = `-${documentHeight - (cardCoordinates.y + cardCoordinates.height - 10)}px`;
    }

    return ( 
        <div className={`card ${cardOpen ? 'card_open' : ''}`} onClick={ !cardOpen ? openCard : () => {}}>
            <div ref={card} className="card__background" style={{backgroundImage: "url(" + img + ")"}}>
                <div className="card__background-shadow">
                    <div className="card__background-text">
                        переходим на - <span>{link}</span> 
                    </div>
                </div>
            </div>
            <div className="card__content">
                <div className="card__title">{title}</div>
                <div className="card__description">{description}</div>
            </div>
        </div>
     );
}

Card.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    link: PropTypes.string,
    img: PropTypes.any
};

Card.defaultProps = {
    title: "pvlvsk.ru",
    description: "asda asd a da sd",
    link: '/',
    img: defaultCard
};
 
export default Card;