import React from 'react';
import PropTypes from 'prop-types';
import ToggleContext from '../../context/Context';

import './ToggleVision.scss'

const ToggleVision = ({children}) => {

    return ( 
        <ToggleContext.Consumer>
        {value => 
            <div className={`toggleVision ${value ? '' : 'toggleVision_activate'}`}>
                {children}
            </div>
        }
        </ToggleContext.Consumer>
     );
}

ToggleVision.propTypes = {
};

ToggleVision.defaultProps = {
};
 
export default ToggleVision;