import React, {useState} from 'react';
import PropTypes from 'prop-types';

import './ScrollPage.scss'

const ScrollPage = (props) => {
    const {upHandler, downHandler, children, delay} = props;

    const [canScroll, setCanScroll] = useState(true);
    const [touchStartСoor, setTouchStartСoor] = useState(0);

    const onScroll = (e) => {
        if (canScroll) {
            if (e.deltaY > 0) scrollEvent(downHandler);
            else if (e.deltaY < 0) scrollEvent(upHandler);
        }
    }

    const TouchStart = (e) => {
        setTouchStartСoor(e.touches[0].clientY);
    }

    const TouchMove = (e) => {
        if (canScroll) {
            if (touchStartСoor > e.touches[0].clientY + 50) {
                scrollEvent(downHandler);
            }
            if (touchStartСoor + 50 < e.touches[0].clientY) {
                scrollEvent(upHandler);
            }
        }
    }

    const scrollEvent = (func) => {
        func();
        scrollLock();
    }

    const scrollLock = () => {
        setCanScroll(false);
        setTimeout(() => {setCanScroll(true)}, delay);
    }

    return ( 
        <div 
            onWheel={onScroll} 
            onTouchStart={TouchStart}
            onTouchMove={TouchMove} 
            onTouchEnd={() => {setTouchStartСoor(0)}} className="scrollPage"
            >
            {children}
        </div>
     );
}

ScrollPage.propTypes = {
    upHandler: PropTypes.func,
    downHandler: PropTypes.func,
    delay: PropTypes.number
};

ScrollPage.defaultProps = {
    upHandler: () => {alert('Scroll UP')},
    downHandler: () => {alert('Scroll DOWN')},
    delay: 1000
};
 
export default ScrollPage;